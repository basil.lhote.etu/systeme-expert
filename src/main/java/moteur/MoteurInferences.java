package moteur;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import facts.*;

/**
 * Le moteur d'inferences
 */
public class MoteurInferences {

    private List<AbstractFact> listFacts;
    private List<Rule> listRules;

    /**
     * Creation du moteur d'inferences.
     *
     * @param facts la base des faits
     * @param rules tla liste des règles
     */
    public MoteurInferences(String facts, String rules) {
        listFacts = FileParser.getFacts(facts);
        listRules = FileParser.getRules(rules);
    }

    /**
     * Obtenir la liste des faits.
     *
     * @return la liste des faits
     */
    public List<AbstractFact> getListFacts(){
        return listFacts;
    }

    /**
     * Obtenir la liste des règles.
     *
     * @return la liste des règles
     */
    public List<Rule> getListRules(){
        return listRules;
    }

    /**
     * Chainage avant.
     */
    public void chainageAvant(){

        for (Rule r : listRules){
            if (r.getConclusion().getEtat() != Etat.INCONNU){
                continue;
            }
            boolean flag = false;
            for (AbstractFact prem : r.getPremisses()){
                if (!listFacts.contains(prem)){
                    flag = true;
                    break;
                }
            }
            if (!flag){
                AbstractFact c = r.getConclusion();
                c.setEtat(Etat.VRAI);
                c.setDeduitDe(r);
                listFacts.add(c);
                chainageAvant();
                break;
            }
        }
    }

    /**
     * Ajoute les règles contraposées.
     */
    public void addContraposees(){
        List<Rule> newRules = new ArrayList<>();
        for (Rule r : listRules){
            if (r.getPremisses().size() == 1){
                AbstractFact newPrem = r.getConclusion().getInverse();
                AbstractFact newConc = r.getPremisses().get(0).getInverse();
                Rule contrapR = new Rule("Contraposée "+r.getDescription(), List.of(newPrem), newConc);
                newRules.add(contrapR);
            }
        }
        listRules.addAll(newRules);
    }

    /**
     * Trouver si une règle a pour conclusion le fait donner.
     *
     * @param fact le fait
     * @return true si une règle a pour conclusion le fait
     */
    public boolean findConclusion(AbstractFact fact){
        for (Rule r : listRules){
            if (r.getConclusion().equals(fact)){
                return true;
            }
        }
        return false;
    }

    /**
     * Trouver la règle qui a pour le conclusion le fait
     *
     * @param fact le fait
     * @return la règle
     */
    public Rule findRule(AbstractFact fact){
        for (Rule r : listRules){
            if (r.getConclusion().equals(fact)){
                return r;
            }
        }
        return null;
    }

    /**
     * Chainage arrière.
     *
     * @param fact le fait
     * @return vrai s'il n'y a pas de conclusion possible.
     */
    public boolean chainageArriere(AbstractFact fact){
        Rule r = findRule(fact);
        if (r != null){
            boolean flag = false;
            for (AbstractFact prem : r.getPremisses()){
                if (!listFacts.contains(prem)){
                    if (findConclusion(prem)) {
                        flag = chainageArriere(prem);
                    } else {
                        System.out.println(prem);
                        System.out.println("vrai ou faux ?");
                        Scanner sc = new Scanner(System.in);
                        boolean resp = sc.nextLine().equals("vrai");
                        if (resp) {
                            prem.setEtat(Etat.VRAI);
                            listFacts.add(prem);
                        } else {
                            flag = true;
                        }
                    }
                }
            }
            if (!flag){
                r.getConclusion().setEtat(Etat.VRAI);
                listFacts.add(r.getConclusion());
                System.out.println(fact+" est vrai");
            } else {
                System.out.println(fact+" est faux");
            }
            return flag;
        }
        System.out.println("Impossible de déterminé une solution pour "+fact);
        return false;
    }
}