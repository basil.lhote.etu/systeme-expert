package moteur;

import facts.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * File Parser, génère les règles et la base de fait à partir des fichiers.
 */
public class FileParser{

    /**
     * Obtenir les faits du fichier.
     *
     * @param FileName nom du fichier
     * @return les faits
     */
    public static List<AbstractFact> getFacts(String FileName) {
        List<AbstractFact> liste = new ArrayList<>();
        try{
            Stream<String> lines = Files.lines(Path.of("src/main/resources/" + FileName));
            lines.forEach(x -> {
                if (x.startsWith("NON")){
                    AbstractFact fact = new Negation(new Fact(x.substring(x.indexOf("(")+1, x.indexOf(")"))));
                    fact.setEtat(Etat.VRAI);
                    liste.add(fact);
                } else {
                    AbstractFact fact = new Affirmation(new Fact(x));
                    fact.setEtat(Etat.VRAI);
                    liste.add(fact);
                }
            });
        } catch (IOException e) {
            System.out.println("Impossible d'accéder au fichier");
            System.out.println(e.getMessage());
        }
        return liste;
    }

    /**
     * Obtenir les règles du fichier.
     *
     * @param FileName le nom du fichier
     * @return les règles
     */
    public static List<Rule> getRules(String FileName){
        List<Rule> liste = new ArrayList<>();
        try{
            BufferedReader br = new BufferedReader(new FileReader("src/main/resources/"+FileName));
            String line;
            Rule r = new Rule();
            while ((line = br.readLine()) != null) {
                if (line.startsWith("REGLE")) {
                    r.setDescription(line.substring(line.indexOf(" ") + 1));
                } else if (line.startsWith("SI") || line.startsWith("ET")) {
                    String factName = line.substring(line.indexOf(" ")+1);
                    if (factName.startsWith("NON")) {
                        r.addPremisse(new Negation(new Fact(line.substring(line.indexOf("(") + 1, line.indexOf(")")))));
                    } else {
                        r.addPremisse(new Affirmation(new Fact(factName)));
                    }
                } else if (line.startsWith("ALORS")) {
                    if (line.contains("NON")) {
                        r.setConclusion(new Negation(new Fact(line.substring(line.indexOf("(") + 1, line.indexOf(")")))));
                    } else {
                        r.setConclusion(new Affirmation(new Fact(line.substring(line.indexOf(" ") + 1))));
                    }
                    liste.add(r);
                    r = new Rule();
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Fichier non trouvé");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("Problème lecture du fichier");
            System.out.println(e.getMessage());
        }

        return liste;
    }
}