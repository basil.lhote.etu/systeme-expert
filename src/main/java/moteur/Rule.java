package moteur;

import facts.AbstractFact;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Une règle
 */
public class Rule {

    private String description;
    private List<AbstractFact> premisses;
    private AbstractFact conclusion;

    /**
     * Création d'une règle
     */
    public Rule(){
        premisses = new ArrayList<>();
    }

    /**
     * Instantiates a new Rule.
     *
     * @param desc  le nom de la règle
     * @param faits les premisses
     * @param conc  la conclusion
     */
    public Rule(String desc, List<AbstractFact> faits, AbstractFact conc){
        description = desc;
        premisses = faits;
        conclusion = conc;
    }

    /**
     * Obtenir le nom de la règle
     *
     * @return le nom de la règle
     */
    public String getDescription() {
        return description;
    }

    /**
     * Changer le nom de la règle
     *
     * @param description le nom
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Obtenir les premisses.
     *
     * @return les premisses
     */
    public List<AbstractFact> getPremisses() {
        return premisses;
    }

    /**
     * Changer les premisses.
     *
     * @param premisses les nouveaux premisses
     */
    public void setPremisses(List<AbstractFact> premisses) {
        this.premisses = premisses;
    }

    /**
     * Obtenir la conclusion.
     *
     * @return la conclusion
     */
    public AbstractFact getConclusion() {
        return conclusion;
    }

    /**
     * Changer la conclusion.
     *
     * @param conclusion nouvelle conclusion
     */
    public void setConclusion(AbstractFact conclusion) {
        this.conclusion = conclusion;
    }

    /**
     * Ajouter un premisse.
     *
     * @param fait le premisse
     */
    public void addPremisse(AbstractFact fait){
        premisses.add(fait);
    }

    @Override
    public String toString() {
        return "Regle "+description+": "+premisses+" alors: "+conclusion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rule rule = (Rule) o;

        return getPremisses().size() == rule.getPremisses().size() && getPremisses().containsAll(rule.getPremisses()) && getConclusion().equals(rule.getConclusion());
    }
}
