package facts;

/**
 * Classe représentant une affirmation d'un fait.
 * Elle étend la classe Decorator.
 */
public class Affirmation extends Decorator {

    /**
     * Constructeur de la classe.
     * Prend en paramètre un objet de type Fact qui sera décoré en affirmation.
     *
     * @param fait L'objet Fact a décorer.
     */
    public Affirmation(Fact fait) {
        faitDecore = fait;
    }

    /**
     * Méthode qui retourne l'inverse du fait affirmé.
     * 
     * @return L'inverse du fait affirmé, sous forme d'un objet Negation.
     */
    @Override
    public AbstractFact getInverse() {
        return new Negation(faitDecore);
    }
}
