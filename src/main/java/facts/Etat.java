package facts;

/**
 * Classe représentant l'état d'un fait.
 * Elle est définie sous forme d'énumération.
 * Les états possibles sont : VRAI, FAUX, INCONNU.
 */
public enum Etat {

    VRAI, FAUX, INCONNU;

    Etat() {
    }
}
