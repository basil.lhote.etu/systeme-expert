package facts;

/**
 * Classe représentant un fait.
 * Elle étend la classe AbstractFact.
 */
public class Fact extends AbstractFact {
    /**
     * Constructeur de la classe.
     * Prend en paramètre une description du fait.
     *
     * @param desc La description du fait.
     */
    public Fact(String desc) {
        nom = desc;
    }

    /**
     * Méthode qui retourne une représentation sous forme de chaîne de caractères du
     * fait.
     *
     * @return Une représentation sous forme de chaîne de caractères du fait.
     */
    public String toString() {
        return super.toString();
    }

    /**
     * Méthode qui retourne l'inverse du fait, qui est null pour la classe Fact.
     *
     * @return null
     */
    @Override
    public AbstractFact getInverse() {
        return null;
    }
}
