package facts;

/**
 * Classe abstraite représentant un décorateur de fait.
 * Elle étend la classe AbstractFact.
 * Ce décorateur permet de décrire un fait de manière différente.
 */

public abstract class Decorator extends AbstractFact {

    protected Fact faitDecore;

    /**
     * Méthode qui retourne le nom du fait décoré.
     *
     * @return Le nom du fait décoré.
     */
    public String getNom() {
        return faitDecore.getNom();
    }

    /**
     * Méthode qui retourne une représentation sous forme de chaîne de caractères du
     * fait décoré.
     *
     * @return Une représentation sous forme de chaîne de caractères du fait décoré.
     */
    @Override
    public String toString() {
        return faitDecore.toString();
    }
}
