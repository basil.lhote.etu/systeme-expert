package facts;

/**
 * Classe représentant une négation d'un fait.
 * Elle étend la classe Decorator.
 */
public class Negation extends Decorator {

    /**
     * Constructeur de la classe.
     * Prend en paramètre un objet de type Fact qui sera décoré négatif.
     *
     * @param fait L'objet Fact a décorer.
     */
    public Negation(Fact fait) {
        faitDecore = fait;
    }

    /**
     * Méthode qui retourne le nom du fait négatif".
     *
     * @return Le nom du fait négatif précédé de "Non: ".
     */
    public String getNom() {
        return "Non: " + faitDecore.getNom();
    }

    /**
     * Méthode qui retourne l'inverse d'un fait négatif.
     *
     * @return L'inverse du fait négatif, sous forme d'un objet Affirmation.
     */
    @Override
    public AbstractFact getInverse() {
        return new Affirmation(faitDecore);
    }

    /**
     * Méthode qui retourne une représentation sous forme de chaîne de caractères du
     * fait négatif.
     *
     * @return Une représentation sous forme de chaîne de caractères du fait
     *         négatif.
     */
    @Override
    public String toString() {
        return "Non: " + super.toString();
    }
}
