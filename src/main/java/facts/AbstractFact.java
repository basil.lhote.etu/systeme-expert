package facts;

import moteur.Rule;

/**
 * Classe abstraite des faits.
 */
public abstract class AbstractFact {

    /**
     * Nom du fait.
     */
    String nom;
    /**
     * Etat du fait.
     */
    Etat etat = Etat.INCONNU;
    /**
     * Règle par laquelle le fait a pu être déduit.
     */
    Rule deduitDe = null;

    @Override
    public String toString() {
        return nom;
    }

    /**
     * Changer l'etat du fait.
     *
     * @param e l'etat
     */
    public void setEtat(Etat e){
        etat = e;
    }

    /**
     * obtenir l'etat du fait.
     *
     * @return l'etat
     */
    public Etat getEtat(){
        return etat;
    }

    /**
     * Obtenir le nom du fait.
     *
     * @return le nom
     */
    public String getNom(){
        return nom;
    }

    /**
     * Changer le règle déduit de.
     *
     * @param r la règle
     */
    public void setDeduitDe(Rule r){
        deduitDe = r;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractFact)) return false;

        AbstractFact that = (AbstractFact) o;

        return getNom().equals(that.getNom());
    }

    /**
     * Obtenir l'inverse de l'affirmation ou la négation.
     *
     * @return l'inverse
     */
    public abstract AbstractFact getInverse();
}
