import facts.Fact;
import moteur.MoteurInferences;

import java.util.Scanner;

public class Main {
    
    public static void main(String[] args) {
        System.out.println("Bienvenue dans notre Système Expert");
        System.out.println("Veuillez renseigner le fichier de fait");
        Scanner sc = new Scanner(System.in);
        String facts = sc.nextLine();
        System.out.println("Veuillez renseigner le fichier de règles");
        String rules = sc.nextLine();
        MoteurInferences mi = new MoteurInferences(facts,rules);
        System.out.println("\nSaisir une commande");
        System.out.println("Pour quitter le système, écrire : exit");
        String commande = sc.nextLine();
        while (!commande.equals("exit")){
            switch (commande){
                case "exit" -> {
                    return;
                } case "start" ->{
                    mi.chainageAvant();
                } case "contraposees" ->{
                    mi.addContraposees();
                } case "chainage arriere" ->{
                    System.out.println("Saisir le fait a conclure");
                    Fact f = new Fact(sc.nextLine());
                    if (mi.findConclusion(f)){
                        mi.chainageArriere(f);
                    } else {
                        System.out.println("Impossible de conclure sur ce fait");
                    }
                }
            }
            System.out.println("Prochaine commande : ");
            commande = sc.nextLine();
        }
    }
}
