import facts.AbstractFact;
import facts.Fact;
import facts.Negation;
import moteur.MoteurInferences;
import moteur.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

public class TestMoteurInferences {
    
    MoteurInferences mi;

    @BeforeEach
    public void init(){
        mi = new MoteurInferences("unExemple-faits.txt", "unExemple-regles.txt");
    }

    @AfterEach
    public void reset(){
        mi = null;
    }

    @Test
    public void testGetListAffirmations(){
        List<AbstractFact> listeFacts = mi.getListFacts();
        assertNotNull(listeFacts);
    }

    @Test
    public void testGetListRules(){
        List<Rule> listRules = mi.getListRules();
        assertNotNull(listRules);
    }

    @Test
    public void testChainageAvant(){
        mi.chainageAvant();
        assertTrue(mi.getListFacts().contains(new Fact("le test fonctionne")));
        assertTrue(mi.getListFacts().contains(new Fact("le test 2 fonctionne")));

    }

    @Test
    public void testContraposees(){
        AbstractFact prem = new Negation(new Fact("Socrate est mortel"));
        AbstractFact conc = new Negation(new Fact("Socrate est un homme"));
        Rule contrap = new Rule("Test", List.of(prem), conc);
        mi.addContraposees();
        assertTrue(mi.getListRules().contains(contrap));
    }
}
