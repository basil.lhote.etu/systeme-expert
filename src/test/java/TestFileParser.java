import moteur.FileParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestFileParser {

    @Test
    public void testFacts(){
        assertEquals("vous avez couru pour échapper au chien", FileParser.getFacts("unExemple-faits.txt").get(0).toString());
        assertEquals("Non: le chien bave", FileParser.getFacts("unExemple-faits.txt").get(1).toString());
        assertEquals("Regle RISQUE DE CONTAGION: [vous avez été mordu, le chien est enragé] alors: vous êtes en danger de mort", FileParser.getRules("unExemple-regles.txt").get(0).toString());
    }
}
