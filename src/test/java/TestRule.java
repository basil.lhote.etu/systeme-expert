import facts.Affirmation;
import facts.Fact;
import moteur.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRule {

    Rule r;

    @BeforeEach
    public void ini(){
        r = new Rule("Règle1", List.of(new Affirmation(new Fact("Le chien bave"))), new Affirmation(new Fact("Le chien est enragé")));
    }

    @AfterEach
    public void reset(){
        r = null;
    }

    @Test
    public void test1(){
        assertEquals("Règle1", r.getDescription());
    }

    @Test
    public void test2(){
        assertEquals("Le chien est enragé", r.getConclusion().toString());
    }

    @Test
    public void test3(){
        assertEquals("Le chien bave", r.getPremisses().get(0).toString());
    }
}
