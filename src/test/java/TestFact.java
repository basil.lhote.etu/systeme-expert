import facts.Affirmation;
import facts.AbstractFact;
import facts.Fact;
import facts.Negation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestFact {

    Fact fait;

    @BeforeEach
    public void init(){
        fait = new Fact("Le chien bave");
    }

    @AfterEach
    public void reset(){
        fait = null;
    }

    @Test
    public void testInconnu(){
        assertEquals("Le chien bave", fait.toString());
    }

    @Test
    public void testAffirmation(){
        AbstractFact aff = new Affirmation(fait);
        assertEquals("Le chien bave", aff.toString());
    }

    @Test
    public void testNegation(){
        AbstractFact neg = new Negation(fait);
        assertEquals("Non: Le chien bave", neg.toString());
    }

}
